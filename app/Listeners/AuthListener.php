<?php

namespace App\Listeners;

use App\Events\AuthEvent;
use Illuminate\Support\Facades\Http;

class AuthListener
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     */
    public function handle(AuthEvent $event): void
    {
        Http::post('https://jsonplaceholder.typicode.com/posts', [
            'title' => 'Login Event',
            'body' => $event->data,

        ]);
    }
}
