<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['path', 'image_type', 'imageable_id', 'imageable_type'];

    public function imageable()
    {
        return $this->morphTo(__FUNCTION__);
    }
}