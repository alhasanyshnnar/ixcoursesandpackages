<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    use HasFactory,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','price','image_id'];
    public function courses()
    {
        return $this->hasMany(Course::class);
    }
    public function purchases()
    {
        return $this->hasMany(Purchase::class);
    }
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}