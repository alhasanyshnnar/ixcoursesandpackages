<?php

namespace App\Services;

use App\Events\AuthEvent;
use App\Models\OtpAttempt;
use App\Models\User;
use Carbon\Carbon;

class UserService
{
    public function signup($request)
    {

        $user = User::create([
            'email' => $request['email'],

        ]);

        return $user;
    }

    public function loginAttempt($request)
    {
        $user = User::where('email', $request['email'])->first();

        // Generate OTP (assuming it's a 6-digit number)
        $otp = mt_rand(100000, 999999);
        $expireAt = Carbon::now()->addMinutes(30);
        // Save OTP attempt in the database
        $otpAttempt = OtpAttempt::create([
            'otp' => $otp,
            'user_id' => $user->id,
            'expire_at' => $expireAt,
        ]);
        AuthEvent::dispatch($otp);

        return $otp;
    }

    public function login($request)
    {
        $user = User::where('email', $request['email'])->first();

        // Check if there is a valid OTP attempt for the user
        $otpAttempt = OtpAttempt::where('user_id', $user->id)
            ->where('otp', $request['otp'])
            ->where('expire_at', '>', Carbon::now())
            ->first();

        if (! $otpAttempt) {
            return false;
        }
        // Optionally, you can clear the used OTP attempt
        $otpAttempt->delete();

        return true;
    }
}
