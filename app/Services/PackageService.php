<?php

namespace App\Services;

use App\Models\Package;

class PackageService
{
    public function index($request)
    {
        $packages = Package::query();

        $packages->when(request()->filled('search'), function ($query) {
            $search = request()->search;
            $query->with('courses')->where('name', 'LIKE', '%'.$search.'%');
        });

        $packages = $packages->latest()->paginate(10);

        return $packages;

    }

    public function store($request)
    {
        $package = package::updateOrCreate(['id' => $request['package_id'] ?? null], $request);

        if (isset($request['image'])) {

            $imagePath = $this->uploadImage($request['image'], $package->id);
            $package->imageable()->delete();
            $package->imageable()->create([
                'path' => $imagePath,
                'image_type' => 'package',
            ]);
        }

        return $package;
    }

    public function delete($id)
    {
        $package = Package::findOrFail($id)->delete();

        return $package;

    }

    protected function uploadImage($image, $packageId)
    {

        $image->store("public/Images/Package/$packageId");

        return "storage/Images/Package/$packageId/".$image->hashName();
    }
}
