<?php

namespace App\Services;

use App\Models\Course;

class CourseService
{
    public function index($request)
    {
        $courses = Course::query();

        $courses->when(request()->filled('search'), function ($query) {
            $search = request()->search;
            $query->where('name', 'LIKE', '%'.$search.'%');
        });

        $courses = $courses->latest()->paginate(10);

        return $courses;

    }

    public function store($request)
    {
        $course = Course::updateOrCreate(['id' => $request['course_id'] ?? null], $request);

        if (isset($request['image'])) {

            $imagePath = $this->uploadImage($request['image'], $course->id);
            $course->imageable()->delete();
            $course->imageable()->create([
                'path' => $imagePath,
                'image_type' => 'course',
            ]);
        }

        return $course;
    }

    public function delete($id)
    {
        $course = Course::findOrFail($id)->delete();

        return $course;

    }

    protected function uploadImage($image, $courseId)
    {

        $image->store("public/Images/Course/$courseId");

        return "storage/Images/Course/$courseId/".$image->hashName();
    }
}
