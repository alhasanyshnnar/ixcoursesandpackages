<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetCourseRequest;
use App\Http\Requests\StoreCourseRequest;
use App\Http\Resources\CourseResource;
use App\Models\Course;
use App\Services\CourseService;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    public function __construct(public CourseService $courseService)
    {

    }

    /**
     * Display a listing of the resource.
     */
    public function index(GetCourseRequest $request)
    {
        $courses = $this->courseService->index($request->validated());

        return response()->json(new CourseResource($courses));

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCourseRequest $request)
    {
        $request = $request->validated();
        $course = $this->courseService->store($request);

        return response()->json(new CourseResource($course));

    }

    /**
     * Display the specified resource.
     */
    public function show(Course $course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Course $course)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Course $course)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $this->courseService->delete($id);

        return 1;

    }
}
