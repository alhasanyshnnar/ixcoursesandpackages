<?php

namespace App\Http\Controllers;

use App\Models\OtpAttemp;
use Illuminate\Http\Request;

class OtpAttempController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(OtpAttemp $otpAttemp)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(OtpAttemp $otpAttemp)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, OtpAttemp $otpAttemp)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(OtpAttemp $otpAttemp)
    {
        //
    }
}
