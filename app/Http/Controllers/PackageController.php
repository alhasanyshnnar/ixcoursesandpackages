<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetPackageRequest;
use App\Http\Requests\StorePackageRequest;
use App\Http\Resources\PackageResource;
use App\Services\PackageService;

class PackageController extends Controller
{
    public function __construct(public PackageService $packageService)
    {

    }

    /**
     * Display a listing of the resource.
     */
    public function index(GetPackageRequest $request)
    {
        $courses = $this->packageService->index($request->validated());

        return response()->json(new PackageResource($courses));

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePackageRequest $request)
    {
        $request = $request->validated();
        $package = $this->packageService->store($request);

        return response()->json(new PackageResource($package));

    }

    public function destroy($id)
    {
        $this->packageService->delete($id);

        return 1;

    }
}
