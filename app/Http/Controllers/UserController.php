<?php

namespace App\Http\Controllers;

use App\Http\Requests\CheckOtpRequest;
use App\Http\Requests\StoreOtpRequest;
use App\Http\Requests\StoreUerRequest;
use App\Services\UserService;

class UserController extends Controller
{
    public function __construct(public UserService $userService)
    {

    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUerRequest $request)
    {
        $request = $request->validated();
        $user = $this->userService->signup($request);

        return response()->json(['message' => 'User registered successfully', 'user' => $user], 201);
    }

    public function loginAttempt(StoreOtpRequest $request)
    {
        $request = $request->validated();
        $otp = $this->userService->loginAttempt($request);

        return response()->json(['message' => 'Check Your Box : '.$otp], 200);
    }

    public function login(CheckOtpRequest $request)
    {
        $request = $request->validated();
        if (! $this->userService->login($request)) {
            return response()->json(['error' => 'Invalid OTP or OTP expired'], 401);
        }

        return response()->json(['message' => 'Login successful'], 200);

    }

    public function logout($request)
    {
        $request->user()->token()->revoke();

        return response()->json(['message' => 'Logout successful']);
    }
}
